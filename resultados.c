#include <stdio.h>
#include <math.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#define PI 3.14159265

int main(){

    double num, resul;


    printf("ingrese el numero a calcular");
    scanf("%lf", &num);

    resul = tan(num*PI/180);
    
    printf("la tangente es %lf \n", resul);

    resul = sin(num*PI/180);

    printf("el seno es %lf \n", resul);

    resul = cos(num*PI/180);

    printf("el coseno es %lf \n", resul);

    return 0;
}